module.exports = {
    publicPath: '/static',

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableLegacy: false,
        globalInjection: true,
        runtimeOnly: false,
        compositionOnly: false,
        legacy: false,
        fullInstall: true
      }
    }
}
