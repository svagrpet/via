export default class IGDBHelper{
	static changeImageQuality(url, desiredQuality) {
		return url.replace('t_thumb', desiredQuality)
	}

	static convertDate(date){
		return new Date(date * 1000);
	}
}

export const IMAGE_QUALITIES = {
	"SmallCover": "t_cover_small",
	"MediumScreenshot": "t_screenshot_med",
	"BigCover": "t_cover_big",
	"MediumLogo": "t_logo_med"
}