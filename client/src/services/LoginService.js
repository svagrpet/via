import axios from 'axios';
import EventBus from '../eventBus';

export default {
	loadedUserData: false,
	loggedIn: false,
	userData: {
		username: null,		
		email: '',				
		games: [],
	},
	init(){		
		if(!this.loadedUserData) this.fetchUserData();
	},
	isLoggedIn(){		
		if(!this.loadedUserData && this.hasSession()) this.fetchUserData();
		return this.loggedIn;
	},	
	fetchUserData() {
		axios.get("/api/user/loggedin").then(response => {
			if(response.headers['content-type'] === 'application/json'){
				this.userData = response.data
				this.loadedUserData = true;
				this.loggedIn = true;
				EventBus.emit('login-update');
			}			
		});
	},

	updateUserData(data) {
		this.loadedUserData = true;
		this.userData = data;
		this.loggedIn = true;
		EventBus.emit('login-update');
	},
	logout(){
		axios.get('/api/user/loggout').then(response => {
			if(response.status == 200){
				this.loadedUserData = false;
				this.userData = {};
				this.loggedIn = false;
				EventBus.emit('login-update');
			}
		})		
	}
};
