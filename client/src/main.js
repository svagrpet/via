import { createApp } from 'vue'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import router from './router'
import i18n from './i18n'
import LoginService from './services/LoginService'
LoginService.init();

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faExternalLinkAlt, faSearch, faExternalLinkSquareAlt, faStore, faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import { faSteam, faFacebook, faInstagram, faYoutube, faTwitter, faTwitch, faItchIo, faWikipediaW, faReddit } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faSearch);
library.add(faCoffee);
library.add(faExternalLinkAlt);
library.add(faExternalLinkSquareAlt);
library.add(faStore);
library.add(faSignInAlt);
//Brands
library.add(faSteam);
library.add(faFacebook);
library.add(faInstagram);
library.add(faYoutube);
library.add(faTwitter);
library.add(faTwitch);
library.add(faItchIo);
library.add(faWikipediaW);
library.add(faReddit);



createApp(App).use(i18n).use(router).component("font-awesome-icon", FontAwesomeIcon).mount('#app');