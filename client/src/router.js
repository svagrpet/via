import {createRouter, createWebHistory} from 'vue-router'

const routes = [
	{
		name: 'root',
		path: '/',
		redirect: {name: 'home'},
	},
	{
		name: 'home',
		path: '/home',
		component: require('@/components/Home').default,
		meta: {
			parent: 'root',
		},
	},
	{
		name: 'login',
		path: '/login',
		component: require('@/components/Login/LoginForm').default,
		meta: {
			parent: 'root',
		},
	},
	{
		name: 'register',
		path: '/register',
		component: require('@/components/Login/RegisterForm').default,
		meta: {
			parent: 'root',
		},
	},
	{
		name: 'search',
		path: '/search',
		component: require('@/components/games/Search').default,
		meta: {
			parent: 'root',
		},
	},
	{
		name: 'game',
		path: '/game/:id',
		component: require('@/components/games/GameDetail').default,
		props: true
	},
	{
		name: 'user.list',
		path: '/user/list',
		component: require('@/components/list/UsersList').default,
		props: true
	},
	{
		path: "/:catchAll(.*)",
		name: "not_found",
		component: require('@/components/404').default }
];


export default createRouter({
	routes: routes,
	linkActiveClass: 'text-secondary',	
	history: createWebHistory(),
});
