from flask import Blueprint, render_template, redirect, url_for, request, jsonify
import requests
from flask_restx import Resource, Namespace, fields


steam = Namespace("Steam", description='Endpoint for communication with steam api')

@steam.route('/game/<id>')
@steam.doc(responses={200: 'OK'}, description="Steam info about game", params={'id': 'Id of the game'})  # Documentation of route
class RedditDeals(Resource):
	def get(self, id):
		resp = requests.get(url='http://store.steampowered.com/api/appdetails?appids={}'.format(id))	
		return resp.json()


