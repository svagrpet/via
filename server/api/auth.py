from flask import Blueprint, render_template, redirect, url_for, request, jsonify
from flask_login import login_user, logout_user, login_required
from flask.wrappers import Response
from werkzeug.security import generate_password_hash, check_password_hash
from models.user import User
from flask_restx import Resource, Namespace, fields
from app import db
from globals import app_api


auth = Namespace('auth', description="Authorization service")

resource_fields_game = auth.model('game', {
	'id': fields.Integer(description='Id of game', default=1),
	'username': fields.String(description='The name of game', default='awesome game'),
	'data': fields.String(description='Json with data if provided', default=''),
})

resource_fields_user = auth.model('User', {
	'id': fields.Integer(description='Id of user', default=1),
    'username': fields.String(description='The username', default='username'),
    'password': fields.String(description='The password hash', default='username'),
    'email': fields.String(description='The email of the user', default='something@asd.com'),
    'games': fields.List(fields.Nested(resource_fields_game))
})

resource_fields_user_login = auth.model('UserForLogin', {
    'email': fields.String(description='The email of the user', default='something@asd.com'),
    'password': fields.String(description='The password of user', default='username')
})

resource_fields_user_register = auth.model('UserForReg', {
    'username': fields.String(description='The username', default='username'),
	'email': fields.Integer(description='The email of the user', default='something@asd.com'),
    'password': fields.String(description='The password of user', default='username')
})



@auth.route('/login')
@auth.doc(responses={200: 'OK'}, description="Login user", body=resource_fields_user_login)
class Login(Resource):
	@auth.marshal_with(resource_fields_user)
	def post(self):
		request_data = request.get_json()
		user = User.query.filter_by(email=request_data['email']).first()
		
		if not user or not check_password_hash(user.password, request_data['password']):			
			return Response("Incorect email or password", status=403, mimetype='application/json') # if the user doesn't exist or password is wrong, reload the page

		# if the above check passes, then we know the user has the right credentials
		login_user(user)		
		ret_user = user.as_dict()
		ret_user["games"] = [game.as_dict() for game in user.games]
		return ret_user		

@auth.route('/signup')
class Signup(Resource):
	@auth.doc(responses={201: 'created', 403:"User already exists"}, description="Login user", body=resource_fields_user_register)
	def post(self):
		user = request.get_json()
		
		if User.query.filter_by(email=user['email']).first(): # if a user is found
			return Response("User already exists", status=403, mimetype='application/json')

		# create a new user with the form data. Hash the password so the plaintext version isn't saved.
		new_user = User(email=user['email'], username=user['username'], password=generate_password_hash(user['password'], method='sha256'))

		# add the new user to the database
		db.session.add(new_user)
		db.session.commit()

		return Response(new_user, status=201, mimetype='application/json')

@auth.route('/logout')
class Logout(Resource):
	@login_required
	def get(self):
		logout_user()
		return Response('ok', status=200, mimetype='application/json')