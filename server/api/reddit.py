from flask import Blueprint, render_template, redirect, url_for, request, jsonify
import requests
from flask_restx import Resource, Namespace, fields

reddit = Namespace("reddit", description='Get data from reddit')

@reddit.route('/deals')
@reddit.doc(responses={200: 'OK'}, description="Get list of game deals")  # Documentation of route
class RedditDeals(Resource):
	def get(self):
		resp = requests.get(url='https://www.reddit.com/r/GameDeals/new/.json?count=10')	
		return resp.json()


