from flask import Flask
from globals import cache
import requests
import os


@cache.cached(timeout=5229828)
def getAuthorization():
	url = 'https://id.twitch.tv/oauth2/token?client_id={}&client_secret={}&grant_type=client_credentials'.format(os.getenv('IGDB_CLIENT_ID'),os.getenv('IGDB_CLIENT_SECRET'))	
	resp = requests.post(url=url)
	return resp.json()
