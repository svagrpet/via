from os import name
from flask import Blueprint, render_template, redirect, url_for, request, jsonify
from flask_login import login_required, current_user
from flask.wrappers import Response
from werkzeug.security import generate_password_hash, check_password_hash
from models.user import User
from flask_restx import Resource, Namespace, fields
import json
from app import db
from models.game import Game


user = Namespace('user', description="Operations with user")

resource_fields_game = user.model('game', {
	'id': fields.Integer(description='Id of game', default=1),
	'username': fields.String(description='The name of game', default='awesome game'),
	'data': fields.String(description='Json with data if provided', default=''),
})

resource_fields_game_only_id = user.model('gameID', {
	'id': fields.Integer(description='Id of game', default=1),
})

resource_fields_user = user.model('User', {
	'id': fields.Integer(description='Id of user', default=1),
    'username': fields.String(description='The username', default='username'),
	'password': fields.String(description='The password hash', default='username'),
    'email': fields.String(description='The email of the user', default='something@asd.com'),
    'games': fields.List(fields.Nested(resource_fields_game))
})

@user.route('/loggedin')
class LoggedIn(Resource):
	@login_required
	@user.doc(description="Get information about logged in user", body=resource_fields_user)
	def get(self):	
		user = current_user.as_dict()
		user["games"] = [game.as_dict() for game in current_user.games]
		return user


@user.route("/games")
class FollowingGames(Resource):
	@login_required
	@user.doc(description="Get list of games for logged in user")
	@user.marshal_list_with(resource_fields_game)
	def get(self):	
		return [game.as_dict() for game in current_user.games]

	@login_required
	@user.doc(responses={200: 'OK'},description="Add game to user's list", body=resource_fields_game)	
	def post(self):	
		request_data = request.get_json()
		search = findGame(current_user.games, request_data["id"])
		if(search is None):
			game = Game(id=request_data["id"], name=request_data["name"], data=request_data["data"], user_id=current_user.id)
			current_user.games.append(game)
			db.session.add(game)
			db.session.commit()
		return Response('ok', status=200, mimetype='application/json')

	@login_required
	@user.doc(responses={200: 'OK', 404:'Not found'},description="Delete game from user's list", body=resource_fields_game_only_id)
	def delete(self):	
		request_data = request.get_json()
		search = findGame(current_user.games, request_data["id"])
		if(search is None):
			return Response('Not found', status=404, mimetype='application/json')
		current_user.games.remove(search)
		db.session.delete(search)
		db.session.commit()
		return Response('ok', status=200, mimetype='application/json')


def findGame(usersGames, id):
	game = [game for game in usersGames if game.id == id]
	if(len(game) == 0):
		return None
	return game[0]

