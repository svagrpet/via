from flask import Blueprint
from flask import current_app

main = Blueprint('main', __name__)

@main.route('/', defaults={'path': ''})
@main.route('/<path:path>')
def catch_all(path):
	return current_app.send_static_file("index.html")