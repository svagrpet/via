from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
db = SQLAlchemy(app)


class User(db.Model):
	__tablename__ = 'user'
	id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
	email = db.Column(db.String(100), unique=True)
	password = db.Column(db.String(200))
	username = db.Column(db.String(400))
	games = db.relationship('Game', backref='Game', lazy='dynamic')

class Game(db.Model):
	__tablename__ = 'game'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(400))
	data = db.Column(db.String(40000))
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


db.create_all()
