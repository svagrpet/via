from app import db
from sqlalchemy import Table, Column, Integer, ForeignKey
import json
class Game(db.Model):
	__tablename__ = 'game'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(400))
	data = db.Column(db.String(40000))
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

	def toJSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

	def as_dict(self):
		return {c.name: getattr(self, c.name) for c in self.__table__.columns}