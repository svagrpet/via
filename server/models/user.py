from flask_login import UserMixin
from sqlalchemy.orm import relationship
from app import db
from models.game import Game
import json

class User(UserMixin, db.Model):
	__tablename__ = 'user'
	id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
	email = db.Column(db.String(100), unique=True)
	password = db.Column(db.String(200))
	username = db.Column(db.String(400))
	games = db.relationship('Game', backref='Game', lazy='select')
	
	def toJSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

	def as_dict(self):
		return {c.name: getattr(self, c.name) for c in self.__table__.columns}

