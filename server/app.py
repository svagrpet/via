import os


from flask_sqlalchemy import SQLAlchemy
from flask_restx import Api, Resource
from flask import Flask, blueprints, jsonify, Blueprint
from flask_cors import CORS
from flask_login import LoginManager
from dotenv import load_dotenv
from flask_caching import Cache
import globals
from dotenv import load_dotenv


# configuration
DEBUG = True

load_dotenv()
# DB
db = SQLAlchemy()
login_manager = LoginManager()

def create_app():
	load_dotenv()
	secret_key = os.getenv('API_SECRET_KEY', '')
	if secret_key == '':
		raise 'API_SECRET_KEY env is missing!'

	# instantiate the app
	globals.app = Flask(__name__, static_folder='../client/dist',static_url_path="/static")
	globals.app.config.from_object(__name__)
	globals.app.config['SECRET_KEY'] = secret_key
	globals.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
	globals.cache = Cache(config={'CACHE_TYPE': 'simple'})
	globals.cache.init_app(globals.app)
	blueprint = Blueprint('api', __name__, url_prefix='/api')
	globals.app_api = Api(blueprint, version="1.0", title="VIA games", description="Application about game deals and games")
	globals.app.register_blueprint(blueprint)
	
	login_manager.login_view = '/login'
	login_manager.init_app(globals.app)
	
	db.init_app(globals.app)    
	
	from models.user import User
	@login_manager.user_loader
	def load_user(id):
		return User.query.get(int(id))

	# enable CORS
	# CORS(app, resources={r'/*': {'origins': '*'}})
	from main import main as main_blueprint
	globals.app.register_blueprint(main_blueprint)

	from api.auth import auth 
	globals.app_api.add_namespace(auth, path="/auth")

	from api.user import user
	globals.app_api.add_namespace(user, path="/user")

	from api.igdb import igdb
	globals.app_api.add_namespace(igdb, path="/igdb")

	from api.reddit import reddit
	globals.app_api.add_namespace(reddit, path="/reddit")	

	from api.steam import steam
	globals.app_api.add_namespace(steam, path="/steam")	

	return globals.app


if __name__ == '__main__':
	app = create_app().run()	
